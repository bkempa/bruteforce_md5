package org.example;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Stream;

import static org.example.UserData.createUserData;


public class FileHandler {
    public static List<String> dictionaryFileReader(String filename) {
        List<String> words = new ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get(filename))) {
            stream.forEach(words::add);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return words;
    }

    public static ConcurrentMap<String, UserData> usersDataFileReader(String filename) {
        ConcurrentMap<String, UserData> usersDataMap = new ConcurrentHashMap<>();

        try (Stream<String> stream = Files.lines(Paths.get(filename))) {
            stream.forEach((line) -> {
                String[] userData = line.split("\\s+");
                UserData userCredentials = null;
                try {
                    userCredentials = createUserData(userData);
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
                Optional.ofNullable(userCredentials).ifPresent(data ->
                        usersDataMap.put(data.getHashedPassword(), data)
                );
            });
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }

        return usersDataMap;
    }
}
