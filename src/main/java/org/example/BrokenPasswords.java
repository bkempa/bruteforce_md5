package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentMap;


public class BrokenPasswords {
    private final List<String> brokenPasswords = new ArrayList<>();
    private final ConcurrentMap<String, UserData> usersDataMap;
    private String lastBrokenPassword;
    private boolean valueSet = false;

    public BrokenPasswords(ConcurrentMap<String, UserData> usersDataMap) {
        this.usersDataMap = usersDataMap;
    }

    public synchronized void addPassword(PasswordData passwordData) {
        while (valueSet) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
        UserData decodedUserData = usersDataMap.remove(passwordData.getHashedPassword());
        brokenPasswords.add(passwordData.getHashedPassword());
        lastBrokenPassword = "Password for " + decodedUserData.getUserMailAddress() + " is " + passwordData.getDecodedPassword();
        valueSet = true;
        notify();
    }

    public synchronized void getLastPassword() {
        while (!valueSet) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
        System.out.println(lastBrokenPassword);
        valueSet = false;
        notifyAll();
    }

    public synchronized void getNumberOfBrokenPasswords() {
        System.out.println(brokenPasswords.size() + " passwords broken.");
        valueSet = false;
        notifyAll();
    }
}
