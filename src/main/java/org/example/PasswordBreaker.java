package org.example;

import lombok.AllArgsConstructor;

import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import static org.example.HashMD5.toMD5;


@AllArgsConstructor
public class PasswordBreaker {
    private final ConcurrentMap<String, UserData> usersDataMap;
    private final BrokenPasswords brokenPasswords;
    private final List<String> dictionary;

    public enum WordCase {
        ALL_UPPER,
        ALL_LOWER,
        FIRST_UPPER
    }

    public void findSimplePasswords(WordCase type) {
            dictionary.stream()
                    .map(word -> {
                        word = switch (type) {
                            case ALL_UPPER -> allLettersUpperCase(word);
                            case ALL_LOWER -> word;
                            case FIRST_UPPER -> firstLetterUpperCase(word);
                        };
                        return new PasswordData(toMD5(word), word);
                    })
                    .filter(possiblePassword -> usersDataMap.containsKey(possiblePassword.getHashedPassword()))
                .forEach(brokenPasswords::addPassword);
    }

    public void findPasswordsWithPostfix(Integer start, Integer end, WordCase type) {
        dictionary.stream()
                .map(word -> switch (type) {
                    case ALL_UPPER -> allLettersUpperCase(word);
                    case ALL_LOWER -> word;
                    case FIRST_UPPER -> firstLetterUpperCase(word);
                })
                .flatMap(word -> IntStream.range(start, end)
                        .mapToObj(number -> word + number)
                        .map(concatWord -> new PasswordData(toMD5(concatWord), concatWord))
                )
                .filter(possiblePassword -> usersDataMap.containsKey(possiblePassword.getHashedPassword()))
                .forEach(brokenPasswords::addPassword);

    }

    public void findPasswordsWithPrefix(Integer start, Integer end, WordCase type) {
        dictionary.stream()
                .map(word -> switch (type) {
                    case ALL_UPPER -> allLettersUpperCase(word);
                    case ALL_LOWER -> word;
                    case FIRST_UPPER -> firstLetterUpperCase(word);
                })
                .flatMap(word -> IntStream.range(start, end)
                        .mapToObj(number -> number + word)
                        .map(concatWord -> new PasswordData(toMD5(concatWord), concatWord))
                )
                .filter(possiblePassword -> usersDataMap.containsKey(possiblePassword.getHashedPassword()))
                .forEach(brokenPasswords::addPassword);
    }

    public void findTwoWordPasswords(String separator) {
        dictionary.stream()
                .flatMap(firstWord -> dictionary.stream()
                        .map(secondWord -> {
                            String concatWord = firstWord + separator + secondWord;
                            return new PasswordData(toMD5(concatWord), concatWord);
                        })
                )
                .filter(possiblePassword -> usersDataMap.containsKey(possiblePassword.getHashedPassword()))
                .forEach(brokenPasswords::addPassword);
    }

    public void findPasswordsWithPrefixAndPostfix(Integer start, Integer end, WordCase type) {
        dictionary.stream()
                .map(word -> switch (type) {
                    case ALL_UPPER -> allLettersUpperCase(word);
                    case ALL_LOWER -> word;
                    case FIRST_UPPER -> firstLetterUpperCase(word);
                })
                .flatMap(word -> IntStream.range(start, end)
                        .mapToObj(firstNumber -> firstNumber + word)
                        .flatMap(wordWithPrefix -> IntStream.range(start, end)
                                .mapToObj(secondNumber -> wordWithPrefix + secondNumber)
                                .map(concatWord -> new PasswordData(toMD5(concatWord), concatWord))
                        )
                )
                .filter(possiblePassword -> usersDataMap.containsKey(possiblePassword.getHashedPassword()))
                .forEach(brokenPasswords::addPassword);
    }

    private void findPasswordsWithNumberReplacement() {
        dictionary.stream()
                .map(word -> {
                    String replacedWord = word.chars()
                            .mapToObj(c -> (char) c)
                            .map(Object::toString)
                            .map(letter -> switch (letter.toLowerCase()) {
                                case "o" -> String.valueOf(0);
                                case "l" -> String.valueOf(1);
                                case "s" -> String.valueOf(5);
                                default -> letter;
                            })
                            .collect(Collectors.joining());
                    return new PasswordData(toMD5(replacedWord), replacedWord);
                })
                .filter(possiblePassword -> usersDataMap.containsKey(possiblePassword.getHashedPassword()))
                .peek(passwordData -> System.out.println(passwordData.getDecodedPassword()))
                .forEach(brokenPasswords::addPassword);
    }

    private String firstLetterUpperCase(String word) {
        for (int i = 0; i < word.length(); i++)
            if (Character.isLetter(word.charAt(i)))
                return word.substring(0, i) + word.substring(i, i+1).toUpperCase() + word.substring(i+1);
        return word;
    }

    private String allLettersUpperCase(String word) {
        return word.toUpperCase();
    }
}
