package org.example;

import lombok.Data;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Data
public class UserData {
    private Long userID;
    private String hashedPassword;
    private String userMailAddress;
    private String username;

    public static UserData createUserData(String[] usersDataArray) throws IOException {
        List<String> listOfUserData = Arrays.stream(usersDataArray).toList();
        if (listOfUserData.size() < 3)
            throw new IOException("User " + listOfUserData.get(0) + " data is not complete");
        UserData userData = new UserData();
        userData.setUserID(Long.valueOf(listOfUserData.get(0)));
        userData.setHashedPassword(listOfUserData.get(1));
        userData.setUserMailAddress(listOfUserData.get(2));
        String username = String.join(" ", listOfUserData
                .subList(3, listOfUserData.size()));
        userData.setUsername(username);

        return userData;
    }
}
