package org.example;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PasswordData {
    private String hashedPassword;
    private String decodedPassword;
}
