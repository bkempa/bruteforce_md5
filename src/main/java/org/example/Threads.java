package org.example;

import lombok.AllArgsConstructor;

import static org.example.PasswordBreaker.WordCase.*;

@AllArgsConstructor
class ProducerSimpleLower extends Thread {
    private final PasswordBreaker passwordBreaker;

    @Override
    public void run() {
        passwordBreaker.findSimplePasswords(ALL_LOWER);
        System.out.println(this.getName() + " finished");
    }
}

@AllArgsConstructor
class ProducerSimpleUpper extends Thread {
    private final PasswordBreaker passwordBreaker;

    @Override
    public void run() {
        passwordBreaker.findSimplePasswords(ALL_UPPER);
        System.out.println(this.getName() + " finished");
    }
}

@AllArgsConstructor
class ProducerSimpleFirstUpper extends Thread {
    private final PasswordBreaker passwordBreaker;

    @Override
    public void run() {
        passwordBreaker.findSimplePasswords(FIRST_UPPER);
        System.out.println(this.getName() + " finished");
    }
}

@AllArgsConstructor
class ProducerPostLower extends Thread {
    private final PasswordBreaker passwordBreaker;

    @Override
    public void run() {
        passwordBreaker.findPasswordsWithPostfix(0, 100, ALL_LOWER);
        System.out.println(this.getName() + " finished");
    }
}

@AllArgsConstructor
class ProducerPostUpper extends Thread {
    private final PasswordBreaker passwordBreaker;

    @Override
    public void run() {
        passwordBreaker.findPasswordsWithPostfix(0, 100, ALL_UPPER);
        System.out.println(this.getName() + " finished");
    }
}

@AllArgsConstructor
class ProducerPostFirstUpper extends Thread {
    private final PasswordBreaker passwordBreaker;

    @Override
    public void run() {
        passwordBreaker.findPasswordsWithPostfix(0, 100, FIRST_UPPER);
        System.out.println(this.getName() + " finished");
    }
}

@AllArgsConstructor
class ProducerPreLower extends Thread {
    private final PasswordBreaker passwordBreaker;

    @Override
    public void run() {
        passwordBreaker.findPasswordsWithPrefix(0, 100, ALL_LOWER);
        System.out.println(this.getName() + " finished");
    }
}

@AllArgsConstructor
class ProducerPreUpper extends Thread {
    private final PasswordBreaker passwordBreaker;

    @Override
    public void run() {
        passwordBreaker.findPasswordsWithPrefix(0, 100, ALL_UPPER);
        System.out.println(this.getName() + " finished");
    }
}

@AllArgsConstructor
class ProducerPreFirstUpper extends Thread {
    private final PasswordBreaker passwordBreaker;

    @Override
    public void run() {
        passwordBreaker.findPasswordsWithPrefix(0, 100, FIRST_UPPER);
        System.out.println(this.getName() + " finished");
    }
}

@AllArgsConstructor
class ProducerPrePostLower extends Thread {
    private final PasswordBreaker passwordBreaker;

    @Override
    public void run() {
        passwordBreaker.findPasswordsWithPrefixAndPostfix(0, 100, ALL_LOWER);
        System.out.println(this.getName() + " finished");
    }
}

@AllArgsConstructor
class ProducerPrePostUpper extends Thread {
    private final PasswordBreaker passwordBreaker;

    @Override
    public void run() {
        passwordBreaker.findPasswordsWithPrefixAndPostfix(0, 100, ALL_UPPER);
        System.out.println(this.getName() + " finished");
    }
}

@AllArgsConstructor
class ProducerPrePostFirstUpper extends Thread {
    private final PasswordBreaker passwordBreaker;

    @Override
    public void run() {
        passwordBreaker.findPasswordsWithPrefixAndPostfix(0, 100, FIRST_UPPER);
        System.out.println(this.getName() + " finished");
    }
}

@AllArgsConstructor
class ProducerSpaceSeparated extends Thread {
    private final PasswordBreaker passwordBreaker;

    @Override
    public void run() {
        passwordBreaker.findTwoWordPasswords(" ");
        System.out.println(this.getName() + " finished");
    }
}

@AllArgsConstructor
class Consumer extends Thread {
    private final BrokenPasswords brokenPasswords;
    @Override
    public void run() {
        while (true) {
            brokenPasswords.getLastPassword();
        }
    }
}
