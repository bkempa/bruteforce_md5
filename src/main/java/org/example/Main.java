package org.example;

import sun.misc.Signal;

import java.io.File;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ConcurrentMap;

import static org.example.FileHandler.usersDataFileReader;
import static org.example.FileHandler.dictionaryFileReader;


public class Main {

    private static PasswordBreaker passwordBreaker;
    private static BrokenPasswords brokenPasswords;
    private static List<String> dictionary;
    private static ConcurrentMap<String, UserData> usersDataMap;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        startProgram("user-data-final.txt");
        while (true) {
            String newDatabaseName = scanner.nextLine();
            String currentWorkingDirectory = new File("").getAbsolutePath() + "/src/test/resources/";
            File f = new File(currentWorkingDirectory + newDatabaseName);
            if(f.exists() && !f.isDirectory()) {
                startProgram(newDatabaseName);
            }
            else
                System.out.println("Provide a correct filename of a file located in " + currentWorkingDirectory + " directory");
        }
    }

    public static void createAndStartThreads() {
        ProducerSimpleLower producerSimpleLower = new ProducerSimpleLower(passwordBreaker);
        ProducerSimpleUpper producerSimpleUpper = new ProducerSimpleUpper(passwordBreaker);
        ProducerSimpleFirstUpper producerSimpleFirstUpper = new ProducerSimpleFirstUpper(passwordBreaker);

        ProducerPostLower producerPostLower = new ProducerPostLower(passwordBreaker);
        ProducerPostUpper producerPostUpper = new ProducerPostUpper(passwordBreaker);
        ProducerPostFirstUpper producerPostFirstUpper = new ProducerPostFirstUpper(passwordBreaker);

        ProducerPreLower producerPreLower = new ProducerPreLower(passwordBreaker);
        ProducerPreUpper producerPreUpper = new ProducerPreUpper(passwordBreaker);
        ProducerPreFirstUpper producerPreFirstUpper = new ProducerPreFirstUpper(passwordBreaker);

        ProducerPrePostLower producerPrePostLower = new ProducerPrePostLower(passwordBreaker);
        ProducerPrePostUpper producerPrePostUpper = new ProducerPrePostUpper(passwordBreaker);
        ProducerPrePostFirstUpper producerPrePostFirstUpper = new ProducerPrePostFirstUpper(passwordBreaker);

        ProducerSpaceSeparated producerSpaceSeparated = new ProducerSpaceSeparated(passwordBreaker);

        Consumer consumer = new Consumer(brokenPasswords);

        producerSimpleLower.start();
        producerSimpleUpper.start();
        producerSimpleFirstUpper.start();

        producerPostLower.start();
        producerPostUpper.start();
        producerPostFirstUpper.start();

        producerPreLower.start();
        producerPreUpper.start();
        producerPreFirstUpper.start();

        producerPrePostLower.start();
        producerPrePostUpper.start();
        producerPrePostFirstUpper.start();

        producerSpaceSeparated.start();

        consumer.start();
    }

    public static void readDictionary(String filepath) {
        dictionary = dictionaryFileReader(filepath);
    }

    public static void readUsersBase(String filepath) {
        usersDataMap = usersDataFileReader(filepath);
    }

    public static void signalHandler() {
        Signal.handle(new Signal("INT"), sig -> {
            System.out.println("SIG" + sig.getName());
            brokenPasswords.getNumberOfBrokenPasswords();
        });
    }

    public static void startProgram(String databaseFilename) {
        String currentWorkingDirectory = new File("").getAbsolutePath() + "/src/test/resources/";
        readDictionary(currentWorkingDirectory + "test-dict-large.txt");
        readUsersBase(currentWorkingDirectory + databaseFilename);
        brokenPasswords = new BrokenPasswords(usersDataMap);
        passwordBreaker = new PasswordBreaker(usersDataMap, brokenPasswords, dictionary);
        createAndStartThreads();
        signalHandler();
    }
}
