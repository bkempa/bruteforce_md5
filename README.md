
# Password breaker

A multi-threaded program for cracking hashed passwords using a dictionary method. Passwords are generated with the Message-Digest 5 cryptographic method. A task of the main thread is to load the list of encrypted passwords for cracking and a dictionary into global memory of the program, and then to run the set of "producer" threads. Their goal is to build possible passwords using specific methods (numeric prefix or postfix of various lengths, two-word combinations, different letter cases) on a basis of a loaded dictionary and to compare their MD5 value with the entire list of encrypted passwords.  If any of them is equal to the created one, the original password is found. It is then sent to the consumer thread that registers the cracked password, deleting it from the global list so that it is no longer being compared. At any time while the program is running it is possible to change the file containing passwords to crack and after receiving the SIGINT signal the consumer thread displays summed up results.


## Features

- Multithreaded program
- MD5 cryptographic algorithm
- Possibility to change passwords base while the program is running
- SIGINT handling (displaying breaking results)
- Cross platform


## Screenshots
#### SIGINT handling
![App Screenshot](https://i.postimg.cc/s2ct83qv/sigint.png)
#### Passwords base changing
![App Screenshot](https://i.postimg.cc/Xv3RQr2R/change-base.png)



## Installation

It is possible to run the program in multiple ways, e.g. by importing it and running it in your IDE or using [Exec Maven](http://www.mojohaus.org/exec-maven-plugin/) plugin. to run it, execute the following command in the console:

```bash
  mvn exec:java -Dexec.mainClass="com.example.Main"
```